package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Optional;

public class Ship implements ModularVessel, CombatReadyVessel {


    @Override
    public PositiveInteger getSize() {
        return null;
    }

    @Override
    public PositiveInteger getCurrentSpeed() {
        return null;
    }

    @Override
    public Optional<AttackAction> attack(Attackable target) {
        return Optional.empty();
    }

    @Override
    public AttackResult applyAttack(AttackAction attack) {
        return null;
    }

    @Override
    public Optional<RegenerateAction> regenerate() {
        return Optional.empty();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void endTurn() {

    }

    @Override
    public void startTurn() {

    }

    @Override
    public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

    }

    @Override
    public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

    }
}
