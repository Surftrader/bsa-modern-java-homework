package com.binary_studio.dependency_detector;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {

		List<String> librariesList = libraries.libraries;
		List<String[]> dependenciesList = libraries.dependencies;

		if (librariesList.isEmpty() || dependenciesList.isEmpty()) {
			return true;
		}

		Set<String> resultSet = new LinkedHashSet<>();
		int count = 0;
		for (int i = 0; i < dependenciesList.size(); i++) {
			String[] list = dependenciesList.get(i);
			for (int j = 0; j < list.length; j++) {
				resultSet.add(list[j]);
				count++;
			}
		}

		if (librariesList.size() >= dependenciesList.size() * count / resultSet.size()) {
			return true;
		}

		return false;
	}

}
