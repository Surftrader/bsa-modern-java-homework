package com.binary_studio.academy_coin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		List<Integer> list = prices.collect(Collectors.toList());
		int result = 0;
		int size = list.size();
		for (int i = 1; i < size; i++) {
			if (list.get(i - 1) < list.get(i)) {
				int diff = list.get(i) - list.get(i - 1);
				result += diff;
			}
		}
		return result;
	}

}
